package pl.pmajewski.pretius.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface CurrencyStorage {

    void put(String currencyCode, BigDecimal value);

    Optional<BigDecimal> get(String currencyCode);

    String baseCurrencyCode();

    List<String> availableCurrencies();
}
