package pl.pmajewski.pretius.repository;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class PLNBasedStorage implements CurrencyStorage {

    public static final String BASE_CURRENCY = "PLN";

    private Map<String, BigDecimal> values = new ConcurrentHashMap<>();

    public PLNBasedStorage() {
        values.put(BASE_CURRENCY, new BigDecimal("1"));
    }

    @Override
    public void put(String currencyCode, BigDecimal value) {
        values.put(currencyCode, value);
    }

    @Override
    public Optional<BigDecimal> get(String currencyCode) {
        return Optional.ofNullable(values.get(currencyCode));
    }

    @Override
    public String baseCurrencyCode() {
        return BASE_CURRENCY;
    }

    @Override
    public List<String> availableCurrencies() {
        return values.keySet().stream().collect(Collectors.toList());
    }
}
