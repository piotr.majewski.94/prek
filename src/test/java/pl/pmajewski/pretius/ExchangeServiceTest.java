package pl.pmajewski.pretius;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.pmajewski.pretius.repository.CurrencyStorage;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

class ExchangeServiceTest {

    public static final String BASE_CURRENCY = "PLN";
    private ExchangeService exchangeService;
    private CurrencyStorage currencyStorage;

    @BeforeEach
    void init() {
        currencyStorage = mock(CurrencyStorage.class);
        when(currencyStorage.get(BASE_CURRENCY)).thenReturn(Optional.of(new BigDecimal(1)));
        when(currencyStorage.baseCurrencyCode()).thenReturn(BASE_CURRENCY);

        exchangeService = new ExchangeService(currencyStorage);
    }

    @Test
    void shouldExchangeFromPlnToUSD() {
        when(currencyStorage.get("USD")).thenReturn(Optional.of(new BigDecimal("2")));
        BigDecimal result = exchangeService.computeExchangedValue("1", BASE_CURRENCY, "USD");

        assertThat(result)
                .isEqualTo(new BigDecimal("0.50"));
    }

    @Test
    void shouldExchangeFromUSDToPLN() {
        when(currencyStorage.get("USD")).thenReturn(Optional.of(new BigDecimal("3.5")));
        BigDecimal result = exchangeService.computeExchangedValue("1", "USD", BASE_CURRENCY);

        assertThat(result)
                .isEqualTo(new BigDecimal("3.50"));
    }

    @Test
    void shouldExchangeFromUSDToGBD() {
        when(currencyStorage.get("USD")).thenReturn(Optional.of(new BigDecimal("2")));
        when(currencyStorage.get("GBP")).thenReturn(Optional.of(new BigDecimal("4")));
        BigDecimal result = exchangeService.computeExchangedValue("2", "USD", "GBP");

        assertThat(result)
                .isEqualTo(new BigDecimal("1.00"));
    }

    @Test
    void shouldExchangeFromGBPToUSD() {
        when(currencyStorage.get("USD")).thenReturn(Optional.of(new BigDecimal("2")));
        when(currencyStorage.get("GBP")).thenReturn(Optional.of(new BigDecimal("4")));
        BigDecimal result = exchangeService.computeExchangedValue("2", "GBP", "USD");

        assertThat(result)
                .isEqualTo(new BigDecimal("4.00"));
    }
}